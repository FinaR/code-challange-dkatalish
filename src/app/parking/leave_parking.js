let expect = require('chai').expect;
let parkinglot = require('../parkinglot');

describe('leave 4', async function () {
    it('should free slot no 4', async function() {
        var preResult = 'await number 4 is free';
        var result = await parkinglot.createParkingLot(6);
        console.log(result);
        expect(result).to.be.equal(preResult);
    });
});